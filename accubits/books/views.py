from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from books.models import Books, BookUser
from books.permissions import UserIsOwnerTodo
from books.serializers import BookSerializer


class BookListCreateAPIView(ListCreateAPIView):
	
	serializer_class = BookSerializer

	def get_queryset(self):
		return BookUser.objects.filter(user=self.request.user)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)


class TodoDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = BookSerializer
    queryset = Books.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwnerTodo)



