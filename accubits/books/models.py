from __future__ import unicode_literals
from django.conf import settings

from django.db import models
from django.utils.encoding import smart_text as smart_unicode
from django.utils.translation import ugettext_lazy as _


class Books(models.Model):
    name = models.CharField(max_length=255)
    author = models.CharField( max_length=255)
    count = models.IntegerField()
    

    class Meta:
        verbose_name = _("Book")
        verbose_name_plural = _("Books")

    def __str__(self):
        return self.name

class BookUser(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	book = models.ForeignKey(Books, on_delete=models.CASCADE)
	date = models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return "%s: %s" % (self.user.email, self.book.name)