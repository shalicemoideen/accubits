from django.contrib import admin
from books.models import Books, BookUser


class TodoAdmin(admin.ModelAdmin):
    list_display = ("user", "name", "done", "date_created")
    list_filter = ("done", "date_created")


admin.site.register(Books)
admin.site.register(BookUser)

