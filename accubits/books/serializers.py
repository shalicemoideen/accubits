from django.contrib.auth.models import User
from rest_framework import serializers
from books.models import Books, BookUser


class BookUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", "username", "email", "date_joined")


class BookSerializer(serializers.ModelSerializer):
    # user = BookUserSerializer(read_only=True)
    # user = serializers.PrimaryKeyRelatedField(
    #     many=True, read_only=True)

    class Meta:
        model = BookUser
        fields = '__all__'
