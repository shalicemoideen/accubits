from django.urls import path
from books.views import BookListCreateAPIView, TodoDetailAPIView

app_name = 'books'

urlpatterns = [
    path('', BookListCreateAPIView.as_view(), name="list"),
    path('<int:pk>/', TodoDetailAPIView.as_view(), name="detail"),
]
