## Accubits-Django Rest Test

An example Django REST framework project for test driven development.


### API Endpoints

#### Users

* **/api/users/** (User registration endpoint)
* **/api/users/login/** (User login endpoint)
* **/api/users/logout/** (User logout endpoint)


#### Admin panel


* **/completed all models operations


#### Books

* **/api/books/** (Todo create and list endpoint)


### Install 

    pip install -r requirements.txt

### Usage

    python manage.py test


